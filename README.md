# Portfolio-App

Platform for showcasing projects developed by group code254.

## Setup/Installation Requirements

To pull the application and perform changes, kindly perform...

* On GitLab, navigate to the main page of the repository.
* Select download icon.
* Your local clone will be downloaded.

## Technologies Used

* JavaScript and jQuery
* Bootstrap


## Authors

Brenda Okumu

Virginia Ndung'u

William Mbotela

Vivian Opondoh



## License

This project is licensed under the MIT License
